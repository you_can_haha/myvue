const mongoose = require('mongoose')
var Schema = mongoose.Schema

var Schema = mongoose.Schema
var texamPagesSchema = new Schema({
  'examInfo':{ type:Array},
  'setter': { type:String},
  'singleChoiceData':{type:Array,unique:true},
  'multiChoiceData': { type:Array,unique:true},
  'setquestime':{type:String},
  'major':{type:String},
  'examTime': { type: String, default: '01:30:00' },
  "examPageTitle":{type:String},
  'examStartTime':{type:Date},
  'testType': { type:String},
  'judgeQusMsg':{type:Array},
  'fillingMsg':{type:Array},
  'testStatus':{type:Boolean}
})

module.exports = mongoose.model('examPages', texamPagesSchema )