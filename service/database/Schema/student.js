const mongoose = require('mongoose')
var Schema = mongoose.Schema
//学生数据结构及与数据库表的连接 
var studentSchema = new Schema({
  userName: { type: String,unique:true },
  userpwd: { type: Number },
  studentnum: { type: Number },
  mobile: { type: Number },
  mail: { type: String },
  grade: { type: String },
  gender: { type: String },
  major: { type: String },
  date: { type: String },
})
module.exports = mongoose.model('students', studentSchema)


