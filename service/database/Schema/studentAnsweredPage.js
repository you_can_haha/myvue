const mongoose = require('mongoose')
var Schema = mongoose.Schema

var Schema = mongoose.Schema
  answeredPagesSchema = new Schema({
    'exampageId':{type:String},
    'examPageInfo':{type:Array},
    'singleStudentAnswered':{type:Array},
    'multiStudentAnswered':{type:Array},
     'valueHalf':{type:Number},
     'StudentWords':{type:String},
     'setter':{type:String},
     'answeredTime':{type:String},
     'whoAnswered':{type:String},
     'scare':{type:Number},
    'sum1': { type: Number},
    'sum2': {type: Number},
    'examTime':{type:Date},
    'examPageTitle': { type: String},
    'testType':{type:String}
})

module.exports = mongoose.model('studentAnsweredPage', answeredPagesSchema)