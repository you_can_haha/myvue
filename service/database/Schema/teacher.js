const mongoose = require('mongoose')
var Schema = mongoose.Schema

//教师数据结构及数据表连接

var Schema = mongoose.Schema
var teacherSchema = new Schema({
  teacherName: { type: String,unique:true },
  teacherId: { type: Number },
  teacherPwd: { type: Number },
  mobile: { type: Number },
  mail: { type: String },
  gender: { type: String },
  major: { type: String },
  project: { type: String },
  date: { type: String },
})
module.exports = mongoose.model('teachers', teacherSchema)