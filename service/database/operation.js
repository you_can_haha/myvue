var Student = require("./Schema/student")
var Teacher=require("./Schema/teacher")
var  examPages=require('./Schema/examPages')
var studentAnsweredPage=require('./Schema/studentAnsweredPage')
var mongodb = require("./test")
const mongoose = require('mongoose')
mongodb()

 
//这部分是用于学生信息处理，如创建一个学生信息，发现符合条件学生，查找所有学生，查找一个，通过id删除学生信息
var CreateNewUser = function (newUser){
  Student.create(newUser, (err, res) => {
    if (err) return console.log(err)
  }) 
}
var Find=(condition,callback)=>{
  Student.find(condition,(error,docs)=>{
    if(error){
    }else{
      return callback(docs)
    }
  })
}

var FindAll=(callback)=>{
  Student.find((error,docs)=>{
     if(error){
     }else{
       return callback(docs)
     }
   })
}
var FindOne=(conditions,callback)=>{
  Student.findOne(conditions,(error,docs)=>{
    if(error){
        return callback(error)
    }else{
       return callback(docs)
    }
  })
}
var Remove=(conditions)=>{
  Student.remove({ _id: { $in:conditions }},(error)=>{
     if(error){
       console.log(error)
     }else{
       console.log('remove ok')
     }
})}

//这部分是用于老师信息处理
var CreateNewTeacher = function (newUser) {
  Teacher.create(newUser, (err, res) => {
    if (err) return console.log(err)
  })
}
var FindT = (condition, callback) => {

  Teacher.find(condition, (error, docs) => {
    if (error) {
    } else {
      return callback(docs.length)
    }
  })
}
var FindAllT = (callback) => {
  Teacher.find((error, docs) => {
    if (error) {
    } else {
      return callback(docs)
    }
  })
}
var FindOneT = (conditions, callback) => {
  Teacher.findOne(conditions, (error, docs) => {
    if (error) {
      return callback(error)
    } else {
      return callback(docs)
    }
  })
}
var RemoveT = (conditions) => {
  Teacher.remove({ _id: { $in: conditions } }, (error) => {
    if (error) {
      console.log(error)
    } else {
      console.log('remove ok')
    }
  })
}


//这部分是用于存考卷内容的操作
var CreateNewExamPage = function (newExamPage){
  examPages.create(newExamPage, (err,res)=>{
    if(err) return console.log(err)
  })
}
var FindTeacherExamMsg=(condition,callback) => {
  examPages.find(condition,(error,docs) =>{
    if (error){

    }else{
      callback(docs)
    
    }
  }) 
}
/* FindTeacherExamMsg({ setter: '黄剑群' },function(msg){
  console.log(msg)
}) */
var RemoveExamPage = (conditions) => {
  examPages.remove({ _id: { $in: conditions } }, (error) => {
    if (error) {
      console.log(error)
    } else {
      console.error('remove ok')
    }
  })
}

var FindByIdAndUpdate_Exam = function (id, testStatus,callback) {
  let _id=mongoose.Types.ObjectId(toString(id))
  examPages.findByIdAndUpdate({ '_id': _id }, { $set: { 'testStatus': testStatus}}, (error, result) => {
    if (error) {
      console.log(error)
    } else {
     callback(result)}})
}






     /* 这一部分用于存考生考试试卷信息 */
var  SaveStudentAnsweredPage=function(a){
        studentAnsweredPage.create(a,(err,res)=>{
        if (err) return console.log(err)
     })
   }

var FindAllStudentScores = (callback)=>{
  studentAnsweredPage.find((error, docs) => {
    if (error) {
      console.log(error)
    } else {
      return callback(docs)
    }
})}


var FindOneStudentAnsweredPage=(condition,callback)=>{
  studentAnsweredPage.find(condition,(err,data)=>{
    if(err){
      console.log(err)
    }else{
      callback(data)
    }
  })
}

var RemoveStudentAnswerPage = (conditions)=>{
  studentAnsweredPage.deleteOne({ _id: { $in: conditions } },err=>{
    if(err){
      console.log(err)
    }else{
      console.log('delect ok')
    }
  }) 

}
var FindOneTeacherExamedMsg = (condition, callback) => {
  studentAnsweredPage.find(condition, (error, docs) => {
    if (error) {

    } else {
      callback(docs)

    }
  })
}








exports.CreateNewUser = CreateNewUser
exports.Find = Find
exports.FindAll= FindAll
exports.Remove = Remove
exports.FindOne= FindOne

exports.CreateNewTeacher = CreateNewTeacher
exports.FindT = FindT 
exports.FindAllT = FindAllT
exports.FindOneT = FindOneT
exports.RemoveT = RemoveT

exports.CreateNewExamPage = CreateNewExamPage
exports.FindTeacherExamMsg = FindTeacherExamMsg
exports.RemoveExamPage = RemoveExamPage
exports.FindByIdAndUpdate_Exam = FindByIdAndUpdate_Exam



exports.SaveStudentAnsweredPage = SaveStudentAnsweredPage
exports.FindOneStudentAnsweredPage = FindOneStudentAnsweredPage
exports.RemoveStudentAnswerPage = RemoveStudentAnswerPage
exports.FindAllStudentScores = FindAllStudentScores 
exports.FindOneTeacherExamedMsg = FindOneTeacherExamedMsg 

