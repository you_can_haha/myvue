  function mongdb() {
  const mongoose = require('mongoose')

 
  const db = "mongodb://localhost/itcast"
    mongoose.connect(db, { useNewUrlParser: true})

    setTimeout(() => {
      mongoose.disconnect()
    }, 2000);

  mongoose.Promise = global.Promise
  
  mongoose.connection.on("error", () => {
    console.log("数据库错误 正在重连")
    mongoose.connect(db)
  })
  mongoose.connection.on("open", () => {
    console.log("数据库连接成功")
  })
  mongoose.connection.on("disconnected", () => {
      console.log("数据库断开连接 正在重连")
      mongoose.connect(db)
    })
}

module.exports = mongdb



 

