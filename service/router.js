var express=require('express')
var router=express.Router()
var a=require('./database/operation')



router
    //学生信息处理路由
    .post('/CreateNewUser',(req,res)=>{
        let date =req.body.date.substr(0,10)
        req.body.date=date
        a.CreateNewUser(req.body)
    })
    .post('/login',(req,res)=>{
         a.Find(req.body,function(a){
             if(a.length>=1){
                 res.send({ status: "oklogin", studentName: '' + req.body.userName })
             } 
             else{
                 res.send({ status: "stoplogin"})  
             }  
        })
    })
    .post('/FindSomeStudents', (req, res) => {
        a.Find(req.body, function (msg) {
            res.send(msg)
        })
    })
    .post('/FindAll',(req,res)=>{
     a.FindAll(function(x){
         res.send(x) })
    })
   .post('/FindOne',(req,res)=>{
       a.FindOne(req.body,function(msg){
          res.send(msg)
       })
    })
    .post('/Remove',(req,res)=>{
         a.Remove(req.body)
    })

   
    //教师信息路由处理
    .post('/CreateNewTeacher', (req, res) => {
        a.CreateNewTeacher(req.body)
    })
    .post('/Teacherlogin', (req, res) => {
        a.FindT(req.body, function (a) {
            if (a >= 1) {
                res.send({ status: "oklogin",teacherName:''+req.body.teacherName})
            }
            else {
                res.send({ status: "stoplogin"})
            }
        })
    })
    .post('/FindAllT', (req, res) => {
        a.FindAllT(function (x) {
            res.send(x)
        })
    })
    .post('/FindOneT', (req, res) => {
        a.FindOneT(req.body, function (msg) {
            res.send(msg)
        })
    })
    .post('/RemoveT', (req, res) => {
        a.RemoveT(req.body)
    })
    //考卷信息路由处理
    .post('/CreateNewExamPage',(req,res)=>{
        console.log(req.body)
        a.CreateNewExamPage(req.body)
    })
    .post('/FindTeacherExamMsg',(req,res)=>{
        a.FindTeacherExamMsg(req.body,(msg)=> {
                res.send(msg)       
        })
    })
    .post('/RemoveExamPage',(req,res)=>{
        a.RemoveExamPage(req.body)
    })
    //学生答题试卷信息
    .post('/SaveStudentAnsweredPage',(req,res)=>{
        a.SaveStudentAnsweredPage(req.body)
        res.send('success')
    })
    .post('/FindOneStudentAnsweredPage',(req,res)=>{
        a.FindOneStudentAnsweredPage(req.body,function(data){
            if (data.length > 8) {
                let newData = data.slice(-7, -1)
                res.send(newData)
            } else {
                res.send(data)
            }
        })

    })
    .post('/RemoveStudentAnswerPage',(req,res)=>{
        a.RemoveStudentAnswerPage(req.body)
    })
    .post('/FindAllStudentScores', (req, res) => {
        a.FindAllStudentScores(function (data) {
            if (data.length>10){
                 let newData=data.slice(-11,-1)    
                    res.send(newData)
            }else{
                 res.send(data)
            }
           
        })
    })
    .post('/FindOneTeacherExamedMsg', (req, res) => {
        a.FindOneTeacherExamedMsg (req.body,(msg)=>{
            res.send(msg)
        })
    })

    .get('/FindByIdAndUpdate_Exam', (req, res) => {
        console.log(req.query)
        console.log(req.query.id)
        console.log(req.query.testStatus)
        a.FindByIdAndUpdate_Exam(req.query.id, req.query.testStatus,rel=>{
           
            res.json({
                'statuss':1,
                'data':rel
            })
        })
    
    })
  
    
 

module.exports=router