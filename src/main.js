import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'
import echarts from 'echarts'
import qs from 'qs'
import './assets/icon/iconfont.css'
import './assets/icon/iconfont.js'
Vue.prototype.$qs=qs
//axios.defaults.headers['Content-Type']='application/x-www-form-urlencoded'
Vue.prototype.$axios = axios
Vue.prototype.$echarts = echarts

//全局http url api
Vue.prototype.urlpath ='localhost:3000'

Vue.prototype.getNowFormatDate = function () {
  var date = new Date();
  var seperator1 = "-";
  var year = date.getFullYear();
  var month = date.getMonth() + 1;
  var strDate = date.getDate();
  if (month >= 1 && month <= 9) {
    month = "0" + month;
  }
  if (strDate >= 0 && strDate <= 9) {
    strDate = "0" + strDate;
  }
  var currentdate = year + seperator1 + month + seperator1 + strDate;
  return currentdate;
};
/* 这是iview ui库 */
import ViewUI from 'view-design';
// import style
import 'view-design/dist/styles/iview.css';
Vue.use(ViewUI);
/* 这是xlsx导入 */
import xlsx from 'xlsx'
Vue.use(xlsx)

/* 登录判断 */
router.beforeEach((to,from,next)=>{
  let getStudentName = sessionStorage.getItem('studentName')
  let getTeacherName = sessionStorage.getItem('teacherName')
  let getadmin = sessionStorage.getItem('admin')
  if(to.path!='/login'&&to.path!='/'){
    if (!getStudentName && !getTeacherName && !getadmin){
      this.$Message.error('抱歉您还未登陆!')
      router.push({
      path:'/'
      })
    }else{
      next()
    }
  }else{
    next()
  }

})

/**
 * 判断两个数组中的元素是否相同（元素顺序无关）
 * @param  {[type]} array [description]
 * @return {[type]}       [description]
 */
Array.prototype.equals = function (array) {
  // if the other array is a falsy value, return
  if (!array)
    return false;
  // compare lengths - can save a lot of time 
  if (this.length != array.length)
    return false;
  for (var i = 0, l = this.length; i < l; i++) {
    // Check if we have nested arrays
    if (this[i] instanceof Array && array[i] instanceof Array) {
      // recurse into the nested arrays
      if (!this[i].equals(array[i]))
        return false;
    }
    else if (!(array.indexOf(this[i]) >= 0)) {
      // Warning - two different object instances will never be equal: {x:20} != {x:20}
      return false;
    }
  }
  return true;
}



Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
