import Vue from 'vue'
import VueRouter from 'vue-router'
import login from '../views/student/login.vue'
import Admin from '../views/admin/admin.vue'
import StudentAnswerPage from '../views/student/studentAnswerPage/studentAnswerPage.vue'
import AdminLogin from '../views/admin/adminLogin.vue'
import TeacherPage from '../views/teacher/TeacherPage.vue'
import StudentPage from '../views/student/studentAnswerPage/studentPage.vue'
import CheckExamPage from '../views/teacher/studentAnsweredInfo/checkExamPage.vue'
import Transcript from '../views/student/studentAnswerPage/transcript.vue'
import AdminFirst from '../views/admin/adminFirst.vue'
import Test from '../views/Test/Test.vue'

Vue.use(VueRouter)

const routes = [
  { path: '/', redirect: '/login' },
  { path: '/login',name:'login', component: login },
  { path: '/Test', name: 'Test', component: Test },
  { path: '/adminLogin', component: AdminLogin },
  { path: '/Admin', name: 'Admin', component: Admin },
  { path: '/AdminFirst', name: 'AdminFirst', component: AdminFirst },
  { path: '/studentAnswerPage', component: StudentAnswerPage },
  { path: '/studentPage', component: StudentPage },
  { path: '/teacherPage', component: TeacherPage },
  { path: '/checkExamPage', name: 'checkExamPage', component: CheckExamPage },
  { path: '/Transcript', name: 'Transcript', component: Transcript },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
